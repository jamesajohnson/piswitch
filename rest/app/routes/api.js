const pg = require('pg');
const express = require('express');
const router = express.Router({mergeParams: true});

/**
* Middleware for every request.
*/
router.use(function(req, res, next) {
    next();
})

router.get('/', (req, res) => {
    res.send(`88888888ba   88   ad88888ba                       88                      88     	   `);      
	res.send(`88      "8b  ""  d8"     "8b                      ""    ,d                88         `);  
	res.send(`88      ,8P      Y8,                                    88                88         `);  
	res.send(`88aaaaaa8P'  88   Y8aaaaa,    8b      db      d8  88  MM88MMM  ,adPPYba,  88,dPPYba, `);  
	res.send(`88""""""'    88     """""8b,   8b    d88b    d8'  88    88    a8"     ""  88P'    "8a`);
	res.send(`88           88           8b    8b  d8' 8b  d8'   88    88    8b          88       88`);
	res.send(`88           88  Y8a     a8P     8bd8'   8bd8'    88    88,   "8a,   ,aa  88       88`);
	res.send(`88           88   "Y88888P"       YP      YP      88    "Y888   "Ybbd8"'  88       88`);
})

// routes
router.use('/devices', require('./devices'));

module.exports = router;
