const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')
var sys = require('sys')
var exec = require('child_process').exec;
var child;

router.route('/:deviceId/status')
    .get(function(req, res) {
        child = exec(`sudo gpio read ${req.params.deviceId}`, function (error, stdout, stderr) {
            //get, switch and write pin state
            res.status(200).send(stdout[0]);
            if (error !== null) {
                console.log(error);
                res.status(400).json(error);
            }
        });
    });

router.route('/:deviceId/switch')
    .get(function(req, res) {
        //set pin mode out
        child = exec(`sudo gpio mode ${req.params.deviceId} out`, function (error, stdout, stderr) {
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });

        let val = 1;
        child = exec(`sudo gpio read ${req.params.deviceId}`, function (error, stdout, stderr) {
            //get, switch and write pin state
            val = stdout;
            val ^= 1;
            child = exec(`sudo gpio write ${req.params.deviceId} ${val}`, function (error, stdout, stderr) {
                res.status(200).json('updated');
                if (error !== null) {
                    console.log(error);
                    es.status(400).json(error);
                }
            });
            if (error !== null) {
                console.log(error);
                res.status(400).json(error);
            }
        });
    });

module.exports = router;
